﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public abstract class BaseDeployable : MonoBehaviour
{

    public float maxHealth = 100;
    protected float m_curHealth = 100;

    public float maxUpgrade = 100;
    protected float m_curUpgrade = 0;

    public float upgradeSpeed = 10f;
    public float damage = 10f;

    public float occupyRadius = 10f;

    public float attackRange = 20f;

    public LayerMask mask;
    public AudioClip[] shotClips;

    public int curLevel = 0;
    public int maxLevel = 1;

    public float cooldown = 0.1f;
    protected float m_lastAttack;

    public string displayName = "Deployable";
    public GameObject projectileToUse;

    protected bool m_isDeployed;

    protected BaseHostile m_currentTarget;
    protected float m_currentTargetDistance;

    protected AudioSource m_AudioSource;

    public abstract bool FindTarget();
    public abstract bool UpdateTarget();
    public abstract bool TryAttack();
    public abstract void BeIdle();
    public abstract void Upgrade();

    // Use this for initialization
    public virtual void Start()
    {
        m_isDeployed = false;
        m_currentTarget = null;
        m_lastAttack = cooldown;
        m_AudioSource = GetComponent<AudioSource>();
        m_curUpgrade = 0;
    }

    // Update is called once per frame
    public virtual void FixedUpdate()
    {
        m_lastAttack += Time.deltaTime;

        //if ( isDeployed && curLevel >= 1 )
        //{
        //    //find new target
        //    if( m_currentTarget == null )
        //    {
        //        foreach (var enemy in AIController.GetAIList())
        //        {
        //            //is visible?
        //            RaycastHit hit;
        //            if (Physics.Raycast(m_Head.position, enemy.transform.position + Vector3.up * 3 - m_Head.position, out hit, Mathf.Infinity))
        //            {
        //                if( hit.transform == enemy.transform || hit.transform.root == enemy.transform.root )
        //                {
        //                    if( m_currentTarget == null )
        //                    {
        //                        m_currentTarget = enemy;
        //                        m_currentTargetDistance = Vector3.Distance(m_Head.position, enemy.transform.position);
        //                    }
        //                    else
        //                    {
        //                        float distance = Vector3.Distance(m_Head.position, enemy.transform.position);
        //                        if( distance < m_currentTargetDistance )
        //                        {
        //                            m_currentTarget = enemy;
        //                            m_currentTargetDistance = distance;
        //                        }
        //                    }
        //                }
        //            }
        //        }

        //        //rotate head when idle
        //        if( m_currentTarget == null )
        //        {                    
        //            m_Head.eulerAngles = Vector3.Slerp(m_Head.eulerAngles, new Vector3(0, m_Head.eulerAngles.y + 3f, 0), Time.fixedDeltaTime * 5f);                 
        //        }
        //    }
        //    else
        //    {
        //        if( m_currentTarget.IsDead() )
        //        {
        //            m_currentTarget = null;
        //            return;
        //        }

        //        var enemyPosition = m_currentTarget.transform.position;

        //        //check if target is still visible and alive
        //        RaycastHit hit;
        //        if (Physics.Raycast(m_Head.position, enemyPosition + enemyPosition - m_Head.position, out hit, Mathf.Infinity, mask))
        //        {
        //            if( hit.transform == m_currentTarget.transform || hit.transform.root == m_currentTarget.transform.root)
        //            {
        //                //rotate towards enemy
        //                var direction = (enemyPosition - m_Head.position).normalized;
        //                var rotation = Quaternion.LookRotation(direction);

        //                m_Head.rotation = Quaternion.Slerp(m_Head.rotation, rotation, Time.fixedDeltaTime * 100f);


        //                m_lastAttack -= Time.deltaTime;
        //                if (m_lastAttack <= 0)
        //                {
        //                    m_MuzzleFlash.Play();
        //                    m_AudioSource.PlayOneShot(shotClip);

        //                    //shoot projectile
        //                    var projectile = Instantiate(projectileToUse);
        //                    var bullet = projectile.GetComponent<Bullet>();
        //                    bullet.SetDamage(damage);

        //                    projectile.transform.position = m_MuzzleFlash.transform.position * 1f;
        //                    projectile.GetComponent<Rigidbody>().AddForce(m_Head.transform.forward * bullet.speed);

        //                    m_lastAttack = cooldown;
        //                }
        //            }
        //            else
        //            {
        //                m_currentTarget = null;
        //            }
        //        }
        //    }
        //}
    }

    protected bool CanAttack()
    {
        return m_isDeployed && curLevel >= 1
            && m_lastAttack >= cooldown && m_currentTarget != null;
    }

    public void Deploy()
    {
        m_isDeployed = true;
    }

    public bool IsDeployed()
    {
        return m_isDeployed;
    }

    public float GetCurrentHealth()
    {
        return m_curHealth;
    }

    public float GetMaximumHealth()
    {
        return maxHealth;
    }

    public float GetCurrentUpgrade()
    {
        return m_curUpgrade;
    }

    public void SetCurrentUpgrade(float upgrade)
    {
        m_curUpgrade = upgrade;
    }

    public float GetMaximumUpgrade()
    {
        return maxUpgrade;
    }

    /// <summary>
    /// Callback to draw gizmos that are pickable and always drawn.
    /// </summary>
    public virtual void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireSphere(transform.position, occupyRadius / 2);
    }
}

﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret1 : BaseDeployable
{
    private Transform m_ActiveTurret;
    private Transform m_TraceFrom;
    private Transform m_RotationBase;
    private bool m_NeedsHeadPosition;
    private ParticleSystem[] m_ParticleSystems;    

    public override void BeIdle()
    {
        throw new NotImplementedException();
    }

    bool RaycastForHostile(Vector3 target, BaseHostile hostile)
    {
        RaycastHit hit;
        if (Physics.Raycast(m_TraceFrom.position, target - m_TraceFrom.position, out hit, attackRange, mask))
        {
            if (hit.transform == hostile.transform || hit.transform.root == hostile.transform.root)
            {
                m_currentTarget = hostile;
                return true;
            }
        }

        return false;
    }

    public override bool FindTarget()
    {
        foreach (BaseHostile hostile in AIController.GetAIList())
        {
            if (RaycastForHostile(hostile.GetBodyPosition(), hostile))
            {
                return true;
            }
            else
            {
                if (RaycastForHostile(hostile.GetHeadPosition(), hostile))
                {
                    m_NeedsHeadPosition = true;
                    return true;
                }
                else
                {
                    m_NeedsHeadPosition = false;
                }
            }
        }

        return false;
    }

    public override bool TryAttack()
    {
        if (CanAttack())
        {
            foreach (ParticleSystem ps in m_ParticleSystems)
            {
                ps.Play();
            }

            GameObject go = Instantiate(projectileToUse);
            go.transform.position = m_TraceFrom.transform.position;
            BaseProjectile projectile = go.GetComponent<BaseProjectile>();
            projectile.GetComponent<Rigidbody>().AddForce(m_TraceFrom.forward * projectile.speed);

            m_AudioSource.PlayOneShot(shotClips[UnityEngine.Random.Range(0, shotClips.Length - 1)]);
            m_lastAttack = 0;

            return true;
        }

        return false;
    }

    public override bool UpdateTarget()
    {
        throw new NotImplementedException();
    }

    public override void Upgrade()
    {
        if (curLevel > 1)
        {
            maxHealth += 50f;
            cooldown -= 0.1f;
            damage += 5f;

            m_ActiveTurret.gameObject.SetActive(false);

            UpdateComponents();

            m_ActiveTurret.gameObject.SetActive(true);
        }
    }

    private void UpdateComponents()
    {
        int level = curLevel == 0 ? 1 : curLevel;
        m_ActiveTurret = transform.Find("Level" + level);
        Debug.Assert(m_ActiveTurret != null);
        m_RotationBase = m_ActiveTurret.Find("Base/Pylon/RotationBase");
        Debug.Assert(m_RotationBase != null);
        m_TraceFrom = m_RotationBase.Find("Turret/TraceFrom");
        Debug.Assert(m_TraceFrom != null);
        m_ParticleSystems = m_ActiveTurret.GetComponentsInChildren<ParticleSystem>();
        Debug.Assert(m_ParticleSystems.Length > 0);
    }

    public override void Start()
    {
        base.Start();
        UpdateComponents();
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();

        if (m_isDeployed && FindTarget())
        {
            Vector3 rootpos = m_currentTarget.transform.position;
            m_RotationBase.LookAt(rootpos + Vector3.up * 2);

            TryAttack();
            // m_RotationBase.LookAt(m_NeedsHeadPosition ? m_currentTarget.GetHeadPosition() 
            //     : m_currentTarget.GetBodyPosition());
        }
        else
        {
            m_currentTarget = null;
        }
    }
    public override void OnDrawGizmos()
    {
        base.OnDrawGizmos();

        if (m_TraceFrom != null)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawRay(m_TraceFrom.position, m_TraceFrom.forward * attackRange);
        }
    }

}

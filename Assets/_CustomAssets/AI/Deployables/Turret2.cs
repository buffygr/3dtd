﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret2 : BaseDeployable
{
    public override void BeIdle()
    {
        throw new NotImplementedException();
    }

    public override bool FindTarget()
    {
        throw new NotImplementedException();
    }

    public override bool TryAttack()
    {
        throw new NotImplementedException();
    }

    public override bool UpdateTarget()
    {
        throw new NotImplementedException();
    }

    public override void Upgrade()
    {
        if (curLevel > 1)
        {
            maxHealth += 50f;
            cooldown -= 0.1f;
            damage += 5f;

            var oldTurret = transform.Find("Level" + (curLevel - 1).ToString());
            var newTurret = transform.Find("Level" + curLevel);

            oldTurret.gameObject.SetActive(false);
            newTurret.gameObject.SetActive(true);

        }
    }

    // Use this for initialization
    public override void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    void Update()
    {

    }
}

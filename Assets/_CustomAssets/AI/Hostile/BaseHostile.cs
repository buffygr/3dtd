﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public abstract class BaseHostile : MonoBehaviour {

    public float maxHealth = 100;
    public float maxSpeed = 10;
    public string displayName = "Unnamed Creature";

    protected float m_curHealth;
    protected Transform m_Head;
    protected Transform m_Body;
    protected Transform m_Foot;
    protected NavMeshAgent m_navmeshAgent;
    protected OrbOfPower m_OrbOfPower;
    protected float m_DistanceToOrb;
    protected bool m_isDead;

    public abstract void HandleDamage(float damage);
    public abstract void UpdateBonePositions();

    // Use this for initialization
    public virtual void Start ()
    {
        m_isDead = false;
        m_curHealth = maxHealth;
        m_navmeshAgent = GetComponent<NavMeshAgent>();
        m_OrbOfPower = GameManager.Instance.OrbOfPower;

        m_navmeshAgent.speed = maxSpeed;

        UpdateBonePositions();
	}
	
	// Update is called once per frame
	public virtual void Update ()
    {
        m_DistanceToOrb = Vector3.Distance(transform.position, m_OrbOfPower.transform.position);
	}

    public float GetCurrentHealth()
    {
        return m_curHealth;
    }

    public Vector3 GetBodyPosition()
    {
        return m_Body.position;
    }         

    public Vector3 GetHeadPosition()
    {
        return m_Head.position;
    }

    public Vector3 GetFootPosition()
    {
        return m_Foot.position;
    }
}

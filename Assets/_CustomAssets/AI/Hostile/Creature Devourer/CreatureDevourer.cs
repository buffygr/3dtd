﻿using DG.Tweening;
using UnityEngine;

public class CreatureDevourer : BaseHostile
{
	private Animator m_Animator;
	public float attackRange = 5f;
    public float attackSpeed = 1f;
    private float m_lastAttack;

    public override void HandleDamage(float damage)
    {
        m_curHealth -= damage;
        if (m_curHealth <= 0)
        {
            m_isDead = true;

            EventManager.Instance.OnHostileDied.Invoke(this);
            EventManager.Instance.OnHostileDespawned.Invoke(this);

            //Despawn            
            m_Animator.SetBool("Running", false);
            m_Animator.SetBool("IsDead", true);
            m_navmeshAgent.velocity = Vector3.zero;
        }
    }

    public override void UpdateBonePositions()
    {
        m_Body = transform.Find("Root_M/Spine1_M");
        m_Head = m_Body.Find("Chest_M/Neck_M/Head_M");
        m_Foot = transform;
    }

    // Use this for initialization
    public override void Start()
    {
        base.Start();

		m_Animator = GetComponent<Animator>();
        m_lastAttack = attackSpeed;
    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();

        m_lastAttack += Time.deltaTime;

		if( m_DistanceToOrb <= attackRange )
        {
            if( m_navmeshAgent.hasPath )
            {
                m_navmeshAgent.Stop();
                m_Animator.SetBool("Running", false);
                m_navmeshAgent.velocity = Vector3.zero;
            }

            if( m_lastAttack >= attackSpeed )              
            {
                m_Animator.SetBool("Attacking", true);
                m_lastAttack = 0;
            }
            else
            {
                m_Animator.SetBool("Attacking", false);
            }
        }
        else
        {
            m_navmeshAgent.SetDestination(m_OrbOfPower.transform.position);
            transform.LookAt(m_navmeshAgent.nextPosition);

			m_Animator.SetBool("Running", true);
        }
    }

    public void OnApplyDamage()
    {
        m_OrbOfPower.curHealth -= 50;
    }
}

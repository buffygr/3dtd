﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using System.Reflection;

//created by Player
public class EventManager
{
    //Ensure singleton
    private static EventManager _instance = new EventManager();
    public static EventManager Instance { get { return _instance; } }

    //Custom Event type because UnityEvent is abstract
    public class GameEvent<T> : UnityEvent<T> { }
    public class GameEvent : UnityEvent { }

    //Deployable Events
    public GameEvent OnDeploymentStart = new GameEvent();
    public GameEvent OnDeploymentEnd = new GameEvent();
    public GameEvent OnDeploymentCancel = new GameEvent();
    public GameEvent<BaseDeployable> OnUpgradingStarted = new GameEvent<BaseDeployable>();
    public GameEvent OnUpgradingStopped = new GameEvent();
    public GameEvent OnDeployableSelectionOpen = new GameEvent();
    public GameEvent OnDeployableSelectionClose = new GameEvent();
    public GameEvent<GameObject> OnDeployableSelected = new GameEvent<GameObject>();
    public GameEvent OnDeployableUpgraded = new GameEvent();

    //UI Events
    public GameEvent<SlotId> OnHotbarSlotChanged = new GameEvent<SlotId>();
    public GameEvent OnCountdownFinished = new GameEvent();

    //Hostile Events
    public GameEvent<BaseHostile> OnHostileSpawned = new GameEvent<BaseHostile>();
    public GameEvent<BaseHostile> OnHostileDespawned = new GameEvent<BaseHostile>();
    public GameEvent<BaseHostile> OnHostileDied = new GameEvent<BaseHostile>();

    //Game Events
    public GameEvent OnGameStarted = new GameEvent();
}

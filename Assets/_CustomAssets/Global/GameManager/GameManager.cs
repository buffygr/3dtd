﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour
{

    public float preparationTime = 60;

    private static GameManager _instance;
    private PlayerController m_PlayerController;
    private OrbOfPower m_OrbOfPower;

    public static GameManager Instance
    {
        get { return _instance; }
    }

    public OrbOfPower OrbOfPower { get { return m_OrbOfPower; } }

    // Use this for initialization
    void Awake()
    {
        _instance = this;
        m_PlayerController = FindObjectOfType<PlayerController>();
        m_OrbOfPower = FindObjectOfType<OrbOfPower>();
    }

    void Start()
    {

        EventManager.Instance.OnGameStarted.Invoke();
    }

    // Update is called once per frame
    void Update()
    {
        Cursor.visible = false;
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AIController : MonoBehaviour {

    private List<BaseHostile> m_ListAI;
    private static AIController _instance;

    public static AIController Instance
    {
        get { return _instance; }
    }

    void OnHostileSpawned(BaseHostile ai)
    {
        m_ListAI.Add(ai);
        Debug.Log("AI Spawned");
    }

    void OnHostileDespawned(BaseHostile ai)
    {
        m_ListAI.Remove(ai);
    }

    void Awake()
    {
        EventManager.Instance.OnHostileSpawned.AddListener(OnHostileSpawned);
        EventManager.Instance.OnHostileDespawned.AddListener(OnHostileDespawned);
    }

	// Use this for initialization
	void Start ()
    {
        _instance = this;
        m_ListAI = new List<BaseHostile>();
	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}

    public static List<BaseHostile> GetAIList()
    {
        return _instance.m_ListAI;
    }
}

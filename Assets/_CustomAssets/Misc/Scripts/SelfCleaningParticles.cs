﻿using UnityEngine;
using System.Collections;

public class SelfCleaningParticles : MonoBehaviour {

    private ParticleSystem m_ParticleSystem;

	// Use this for initialization
	void Start () {
        m_ParticleSystem = GetComponentInChildren<ParticleSystem>();
	}
	
	// Update is called once per frame
	void Update () {
        if (!m_ParticleSystem.isPlaying)
            Destroy(gameObject);
	}
}

﻿using UnityEngine;
using System.Collections;

public class Lasersight : MonoBehaviour {

    private LineRenderer m_LineRenderer;

	// Use this for initialization
	void Start () {
        m_LineRenderer = GetComponent<LineRenderer>();
        m_LineRenderer.enabled = true;
	}
	
	// Update is called once per frame
	void Update () {

        const float range = 50f;
        var positions = new Vector3[2]
        {
            transform.position, transform.parent.position + transform.forward * range
        };

        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward * range, out hit, range))
        {
            positions[1] = hit.point;
        }

        m_LineRenderer.SetPositions(positions);
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectSpawner : MonoBehaviour {

    [System.Serializable]
    public class SpawningData
    {
        public GameObject obj;
        public int count;
        public float interval;
    }

    public SpawningData[] Spawns;    

    private bool m_Spawning;
    private float m_CurrentInterval;


	// Use this for initialization
    void OnCountdownFinished()
    {
        m_Spawning = true;
    }

	void Start () {
        m_Spawning = false;
        EventManager.Instance.OnCountdownFinished.AddListener(OnCountdownFinished);
	}
	
	// Update is called once per frame
	void Update ()
    {
        m_CurrentInterval += Time.deltaTime;

	    if( m_Spawning )
        {
            for ( var i = 0; i < Spawns.Length; i++ )
            {
                var data = Spawns[i];

                if (data.count <= 0)
                    continue;

                if (m_CurrentInterval < data.interval)
                    break;

                data.count = data.count - 1;

                m_CurrentInterval = 0;
                GameObject spawn = Instantiate(data.obj, transform.position, Quaternion.identity) as GameObject;

                EventManager.Instance.OnHostileSpawned.Invoke(spawn.GetComponent<BaseHostile>());
            }
        }
	}

    void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireCube(transform.position, Vector3.one);
    }
}

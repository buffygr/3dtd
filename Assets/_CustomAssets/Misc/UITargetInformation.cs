﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UITargetInformation : MonoBehaviour
{
    private Image m_Healthbar;
    private Transform m_Panel;
    private TextMeshProUGUI m_DisplayName;

    // Use this for initialization
    void Start()
    {
        m_Panel = transform.Find("Panel");
        m_Healthbar = m_Panel.Find("Background/Healthbar").GetComponent<Image>();
        m_DisplayName = GetComponentInChildren<TextMeshProUGUI>();
        m_Panel.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        BaseHostile target = PlayerView.Instance.TryGetComponent<BaseHostile>(true);
        if (target != null)
        {
            m_Healthbar.fillAmount = target.GetCurrentHealth() / target.maxHealth;
            m_DisplayName.text = target.displayName;
        }

        m_Panel.gameObject.SetActive(target != null);
    }
}

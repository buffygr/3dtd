﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIOrbDetail : MonoBehaviour
{

    // Use this for initialization
    private OrbOfPower m_Orb;

	private TextMeshProUGUI m_Text;

	private RectTransform m_Healthbar;

    void Start()
    {
		m_Orb = GameManager.Instance.OrbOfPower;
		m_Text = GetComponentInChildren<TextMeshProUGUI>();
		m_Healthbar = transform.Find("Healthbar/HealthBarFG").GetComponent<RectTransform>();
    }

    // Update is called once per frame
    void Update()
    {
		float factor = (m_Orb.curHealth / m_Orb.maxHealth);
		Vector2 maxSize = m_Healthbar.sizeDelta;

		m_Healthbar.sizeDelta = new Vector2(factor * 350, maxSize.y);
    }
}

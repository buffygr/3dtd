﻿using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;


public class UICountdown : MonoBehaviour
{
    private float m_Time;
    public string text = "Prepare!";
    private TextMeshProUGUI m_Text;
    private bool m_Finished;
    private int m_lastInt;
    private AudioSource m_AudioSource;
    public AudioClip[] numberClips;
    public AudioClip clipGo;

    void OnGameStarted()
    {
        m_Time = GameManager.Instance.preparationTime;
        m_Text = GetComponentInChildren<TextMeshProUGUI>();
        m_Finished = false;
        m_lastInt = Mathf.RoundToInt(m_Time);
        UpdateText();
    }

    void Awake()
    {
        EventManager.Instance.OnGameStarted.AddListener(OnGameStarted);
    }

    // Use this for initialization
    void Start()
    {
        m_AudioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (m_Finished)
            return;

        if (m_Time > 0)
        {
            m_Time -= Time.deltaTime;
            int newInt = Mathf.RoundToInt(m_Time);
            if (newInt < m_lastInt)
            {
                m_lastInt = newInt;

                if (m_lastInt <= 10 && m_lastInt > 0)
                {
                    //m_AudioSource.Play();
                    m_AudioSource.PlayOneShot(numberClips[10 - m_lastInt]);
                }
            }

            UpdateText();
        }
        else
        {
            m_AudioSource.PlayOneShot(clipGo);

            m_Time = 0;
            m_Finished = true;

            EventManager.Instance.OnCountdownFinished.Invoke();

            foreach (Image _img in GetComponentsInChildren<Image>())
            {
                Color currentColor = _img.color;
                currentColor.a = 0;

                DOTween.To(() => _img.color, x => _img.color = x, currentColor, 1f);
            }

            DOTween.To(() => m_Text.color, x => m_Text.color = x, new Color(1, 1, 1, 0), 1f).OnComplete(() => Destroy(gameObject));
        }

    }

    void UpdateText()
    {
        m_Text.text = text + "\n" + Mathf.RoundToInt(m_Time).ToString();
        m_Text.enabled = true;
    }
}
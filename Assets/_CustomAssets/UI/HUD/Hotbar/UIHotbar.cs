﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

public enum SlotId
{
    None = 0,
    Slot1 = 1,
    Slot2 = 2
}

public class UIHotbar : MonoBehaviour {

    private static SlotId _currentSlot = SlotId.None;
    public Color active = new Color(0, 0, 0, 0.5f);
    public Color inactive = new Color(1, 1, 1, 0.88f);

    void OnSlotChanged(SlotId slot)
    {
        if (slot == _currentSlot)
            return;

        float fadeTime = 0.25f;        

        UIHotbarSlot[] hotbarSlots = GetComponentsInChildren<UIHotbarSlot>();

        foreach (UIHotbarSlot hotbarSlot in hotbarSlots)
        {
            Image img = hotbarSlot.GetComponent<Image>();            
            DOTween.To(() => img.color, x => img.color = x, inactive, fadeTime);
        }

        int slotId = (int)slot - 1;
        UIHotbarSlot currentSlot = hotbarSlots[slotId];

        var _img = currentSlot.GetComponent<Image>();
        DOTween.To(() => _img.color, x => _img.color = x, active, fadeTime);

        _currentSlot = slot;

        EventManager.Instance.OnDeployableSelected.Invoke(currentSlot.boundPrefab);        
    }

    public static SlotId GetCurrentSlotId()
    {
        return _currentSlot;
    }

    void Awake()
    {
        EventManager.Instance.OnHotbarSlotChanged.AddListener(OnSlotChanged);
    }

	// Use this for initialization
	void Start ()
    {
        OnSlotChanged(SlotId.Slot1);
	}
}

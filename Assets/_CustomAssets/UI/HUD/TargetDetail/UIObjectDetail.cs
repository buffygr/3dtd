﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
using System;

public class UIObjectDetail : MonoBehaviour
{

    public float maxDetailDistance = 10f;
    public Texture2D cursor;
    private static GameObject _currentObject = null;

    private TextMeshProUGUI m_HealthText;
    private TextMeshProUGUI m_SpeedText;
    private TextMeshProUGUI m_DamageText;
    private TextMeshProUGUI m_UpgradeText;
    private Canvas m_ObjectDetailCanvas;
    private RectTransform m_MaxUpgradeBar;
    private RectTransform m_CurUpgradeBar;

    private GameObject m_UpgradeBarParent;

    void SetDetailText(string text)
    {
        GetComponentInChildren<Text>().text = text;
    }

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake()
    {
        EventManager.Instance.OnDeployableUpgraded.AddListener(OnDeployableUpgraded);
    }

    // Use this for initialization
    void Start()
    {
        m_ObjectDetailCanvas = GetComponent<Canvas>();

        Transform stats = transform.Find("Stats");
        m_HealthText = stats.Find("P1/Health").GetComponentInChildren<TextMeshProUGUI>();
        m_SpeedText = stats.Find("P2/Speed").GetComponentInChildren<TextMeshProUGUI>();
        m_DamageText = stats.Find("P3/Damage").GetComponentInChildren<TextMeshProUGUI>();

        m_UpgradeBarParent = transform.Find("UpgradeBar").gameObject;

        m_MaxUpgradeBar = m_UpgradeBarParent.GetComponent<RectTransform>();
        m_CurUpgradeBar = m_UpgradeBarParent.transform.Find("UpgradeBarFG").GetComponent<RectTransform>();
        m_UpgradeText = m_UpgradeBarParent.GetComponentInChildren<TextMeshProUGUI>();

        //Cursor.SetCursor(cursor, Vector2.zero, CursorMode.Auto);

    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, Mathf.Infinity))
        {
            m_ObjectDetailCanvas.enabled = false;

            if (hit.distance <= maxDetailDistance)
            {
                if (hit.collider.transform.root.tag == "Deployable")
                {
                    var deployable = hit.collider.transform.root.GetComponent<BaseDeployable>();
                    if (deployable.IsDeployed())
                    {
                        m_ObjectDetailCanvas.enabled = true;

                        m_HealthText.text = deployable.maxHealth.ToString();
                        m_SpeedText.text = string.Format("{0:0.00}", 1 / deployable.cooldown);
                        m_DamageText.text = deployable.damage.ToString();

                        if (deployable.curLevel == 0)
                        {
                            m_UpgradeText.text = "Inactive";
                        }
                        else if (deployable.curLevel == deployable.maxLevel)
                        {
                            m_UpgradeText.text = "Maximum reached";
                        }
                        else
                        {
                            m_UpgradeText.text = "<sprite index=0> " + deployable.curLevel;// + "/" + deployable.maxLevel;
                        }

                        float factor = deployable.GetCurrentUpgrade() / deployable.GetMaximumUpgrade();
                        Vector2 maxSize = m_MaxUpgradeBar.sizeDelta;

                        m_CurUpgradeBar.sizeDelta = new Vector2(maxSize.x * factor, maxSize.y); //
                    }

                    _currentObject = hit.collider.gameObject;
                }
                else
                {
                    _currentObject = null;
                }
            }
            else
            {
                _currentObject = null;
            }
        }
    }

    public static GameObject GetCurrentObject()
    {
        return _currentObject;
    }

    private void OnDeployableUpgraded()
    {
        Vector3 oldScale = m_UpgradeBarParent.transform.localScale;

        m_UpgradeBarParent.transform.DOScale(oldScale * 1.2f, 0.3f)
            .OnComplete(() => m_UpgradeBarParent.transform.DOScale(oldScale, 0.3f));
    }
}

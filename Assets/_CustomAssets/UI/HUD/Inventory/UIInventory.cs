﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIInventory : MonoBehaviour {

    private Text m_CurrencyText;

    void OnBaseAIDied(BaseHostile ai)
    {
        var currentAmount = PlayerController.Instance.GetCurrencyAmount();
        //currentAmount += ai.currencyReward;

        PlayerController.Instance.SetCurrencyAmount(currentAmount);
        m_CurrencyText.text = currentAmount.ToString();
    }

    void Awake()
    {
        EventManager.Instance.OnHostileDied.AddListener(OnBaseAIDied);
    }

	// Use this for initialization
	void Start ()
    {
        m_CurrencyText = GetComponentInChildren<Text>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIMainMenu : MonoBehaviour {

    public GameObject loadingScreen;
    private AsyncOperation m_LoadingOperation;

	// Use this for initialization
    void Awake()
    {
        DontDestroyOnLoad(transform.root.gameObject);
    }

	void Start ()
    {
        m_LoadingOperation = null;
	}
	
	// Update is called once per frame
	void Update ()
    {
    }

    public void OnStartGame()
    {
        transform.parent.gameObject.SetActive(false);
        loadingScreen.SetActive(true);
        m_LoadingOperation = SceneManager.LoadSceneAsync("_01");
    }
}

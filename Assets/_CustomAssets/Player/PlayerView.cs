﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerView : MonoBehaviour {

    //Ensure singleton
    private static PlayerView _instance;
    public static PlayerView Instance { get { return _instance; } }

    RaycastHit m_LastHit;
    public static RaycastHit Hit
    {
        get { return _instance.m_LastHit; }
    }

    public static GameObject GameObject
    {
        get { return _instance.m_LastHit.collider.gameObject; }
    }

    // Use this for initialization
    void Start ()
    {
        _instance = this;
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        Physics.Raycast(transform.position, transform.forward, out m_LastHit, Mathf.Infinity);
        Debug.DrawRay(transform.position, transform.forward * 10f, Color.red);
	}

    public T TryGetComponent<T> (bool root = false)
    {
        if( m_LastHit.collider != null )
        {
            T ret = root ? m_LastHit.collider.transform.root.GetComponent<T>() 
                : m_LastHit.collider.gameObject.GetComponent<T>();

            if (ret != null)
                return ret;
        }

        return default(T);
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class DeployableManager : MonoBehaviour
{

    public GameObject selectedDeployable;
    public float maxDeployDistance = 3f;
    public float minDeployDistance = 2f;
    public LayerMask mask;
    public AudioClip clipDeployed;

    public Material matB;
    public Material matR;

    private PlayerController m_playerController;
    private RaycastHit m_lastHit;
    private bool m_Deploying;
    private GameObject m_lastDeployed;
    private bool m_isFine;
    private List<BaseDeployable> m_Deployables;
    private BaseDeployable m_UpgradeTarget;

    void Awake()
    {
        EventManager.Instance.OnDeploymentStart.AddListener(OnDeploymentStart);
        EventManager.Instance.OnDeploymentEnd.AddListener(OnDeploymentEnd);
        EventManager.Instance.OnDeploymentCancel.AddListener(OnDeploymentCancel);
        EventManager.Instance.OnDeployableSelected.AddListener(OnDeployableSelected);
        EventManager.Instance.OnUpgradingStarted.AddListener(OnUpgradingStarted);
        EventManager.Instance.OnUpgradingStopped.AddListener(OnUpgradingStopped);

    }

    // Use this for initialization
    void Start()
    {
        m_isFine = true;
        m_Deploying = false;
        m_playerController = FindObjectOfType<PlayerController>();
        m_Deployables = new List<BaseDeployable>();
        m_UpgradeTarget = null;

    }

    // Update is called once per frame
    void Update()
    {
        if (m_UpgradeTarget != null)
        {
            ProcessUpgrade();
        }

        if (m_lastDeployed == null)
            return;

        if (Physics.Raycast(m_playerController.transform.position, Camera.main.transform.forward, out m_lastHit, Mathf.Infinity, mask))
        {
            if (m_Deploying && m_lastDeployed.transform != m_lastHit.transform)
            {
                m_lastDeployed.transform.position = m_lastHit.point;
                m_lastDeployed.transform.localEulerAngles = PlayerController.Instance.transform.localEulerAngles;

                m_isFine = m_lastHit.distance >= minDeployDistance && m_lastHit.distance <= maxDeployDistance;

                //Check if deployable is grounded
                if (m_isFine)
                {
                    Vector3[] directions = new Vector3[] {
                        m_lastDeployed.transform.forward, -m_lastDeployed.transform.forward, m_lastDeployed.transform.right, -m_lastDeployed.transform.right
                    };

                    foreach (Vector3 direction in directions)
                    {
                        RaycastHit groundHit;
                        if (!Physics.Raycast(m_lastDeployed.transform.position + Vector3.up / 2
                            + direction * 1.5f, Vector3.down, out groundHit, 1f, mask))
                        {
                            m_isFine = false;
                            break;
                        }
                    }
                }

                if (m_isFine)
                {
                    //Respect each deployables occupy radius
                    foreach (BaseDeployable deployable in m_Deployables)
                    {
                        m_isFine = m_isFine && Vector3.Distance(m_lastHit.point, deployable.transform.position) >= deployable.occupyRadius;
                        if (!m_isFine)
                            break;
                    }
                }

                //Apply blueprint material
                foreach (var renderer in m_lastDeployed.GetComponentsInChildren<Renderer>())
                {
                    renderer.material = m_isFine ? matB : matR;
                }
            }
        }
    }

    void OnDeployableSelected(GameObject prefab)
    {
        selectedDeployable = prefab;

        if (m_lastDeployed != null)
        {
            Destroy(m_lastDeployed);
            m_lastDeployed = Instantiate(selectedDeployable);
        }

    }

    void OnDeploymentCancel()
    {
        Destroy(m_lastDeployed);
        m_lastDeployed = null;
    }

    void OnDeploymentEnd()
    {
        if (m_isFine && m_lastDeployed != null)
        {
            var renderers1 = m_lastDeployed.GetComponentsInChildren<Renderer>();
            var renderers2 = selectedDeployable.GetComponentsInChildren<Renderer>();

            Vector3 oldScale = m_lastDeployed.transform.localScale;
            m_lastDeployed.transform.localScale = new Vector3(oldScale.x, 0, oldScale.z);

            //Apply original materials
            for (var i = 0; i < renderers1.Length; i++)
            {
                renderers1[i].materials = renderers2[i].sharedMaterials;
            }

            BaseDeployable deployable = m_lastDeployed.GetComponent<BaseDeployable>();

            m_Deployables.Add(deployable);

            GetComponent<AudioSource>().PlayOneShot(clipDeployed);

            //Reactivate colliders after deployment
            foreach (var collider in m_lastDeployed.GetComponentsInChildren<Collider>())
            {
                Physics.IgnoreCollision(collider, m_playerController.GetCollider(), false);
            }

            m_lastDeployed.transform.DOScale(oldScale, 0.5f).SetEase(Ease.Linear).OnComplete(() => deployable.Deploy());
        }
        else
        {
            Destroy(m_lastDeployed);
        }

        m_Deploying = false;
        m_lastDeployed = null;
    }

    void OnDeploymentStart()
    {
        if (selectedDeployable != null)
        {
            if (!m_Deploying)
            {
                m_lastDeployed = Instantiate(selectedDeployable);

                //Deactivate colliders while placing
                foreach (var collider in m_lastDeployed.GetComponentsInChildren<Collider>())
                {
                    Physics.IgnoreCollision(collider, m_playerController.GetCollider());
                }

                foreach (var renderer in m_lastDeployed.GetComponentsInChildren<Renderer>())
                {
                    renderer.material = matB;
                }

                m_Deploying = true;
            }
        }
    }

    void ProcessUpgrade()
    {
        float curUpgrade = m_UpgradeTarget.GetCurrentUpgrade();

        if (m_UpgradeTarget.curLevel < m_UpgradeTarget.maxLevel)
        {
            curUpgrade += m_UpgradeTarget.upgradeSpeed * Time.deltaTime;
            m_UpgradeTarget.SetCurrentUpgrade(curUpgrade);

            if (curUpgrade > m_UpgradeTarget.maxUpgrade)
            {
                curUpgrade = m_UpgradeTarget.maxUpgrade;
                m_UpgradeTarget.curLevel++;

                //increase stats
                m_UpgradeTarget.Upgrade();
                m_UpgradeTarget.SetCurrentUpgrade(0);

                EventManager.Instance.OnDeployableUpgraded.Invoke();
            }
        }
    }

    void OnUpgradingStarted(BaseDeployable deployable)
    {
        m_UpgradeTarget = deployable;
    }

    void OnUpgradingStopped()
    {
        if (m_UpgradeTarget != null)
        {
            m_UpgradeTarget.SetCurrentUpgrade(0);
            m_UpgradeTarget = null;
        }
    }

    /// <summary>
    /// Callback to draw gizmos that are pickable and always drawn.
    /// </summary>
    void OnDrawGizmos()
    {
        if (m_Deploying)
        {
            Vector3[] directions = new Vector3[] {
                m_lastDeployed.transform.forward, -m_lastDeployed.transform.forward, m_lastDeployed.transform.right, -m_lastDeployed.transform.right
            };

            foreach (Vector3 direction in directions)
            {
                Debug.DrawRay(m_lastDeployed.transform.position + Vector3.up / 2 + direction * 1.5f, Vector3.down, Color.red);
            }
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;

public class PlayerController : MonoBehaviour {

    public int startCurrency = 1000;

    private static PlayerController _instance;

    private Animator m_Animator;
    private int m_CurrencyAmount;
    private Collider m_Collider;
    private ParticleSystem m_UpgradeRay;
    private ParticleSystem m_RayImpact;
    private FirstPersonController m_FpsController;

    public GameObject fingerGunProjectile;

    public static PlayerController Instance
    {
        get { return _instance; }
    }

	// Use this for initialization
	void Start ()
    {
        _instance = this;

        m_CurrencyAmount = startCurrency;
        m_Animator = GetComponentInChildren<Animator>();
        m_Collider = GetComponent<Collider>();
        m_UpgradeRay = GameObject.Find("UpgradeRay").GetComponent<ParticleSystem>();
        m_RayImpact = m_UpgradeRay.transform.Find("RayImpact").GetComponent<ParticleSystem>();
        m_FpsController = GetComponent<FirstPersonController>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if( Input.GetMouseButtonDown(0) )
        {
            //m_Animator.SetBool("IsShooting", true);
            m_Animator.Play("Arms@FingerGunShot");
        }

        if ( Input.GetKeyDown(KeyCode.Alpha1) )
        {
            EventManager.Instance.OnHotbarSlotChanged.Invoke(SlotId.Slot1);
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            EventManager.Instance.OnHotbarSlotChanged.Invoke(SlotId.Slot2);
        }

        if (Input.GetKey(KeyCode.E))
        {
            var deployable = PlayerView.Instance.TryGetComponent<BaseDeployable>(true);
            if (deployable != null && deployable.IsDeployed())
            {
                //EventManager.Instance.OnDeployableUpgrading.Invoke(deployable, Time.deltaTime);
                EventManager.Instance.OnUpgradingStarted.Invoke(deployable);

                RaycastHit hit = PlayerView.Hit;
                if( hit.collider != null )
                {
                    m_UpgradeRay.transform.LookAt(hit.point);
                    m_RayImpact.transform.position = hit.point;

                    if (!m_UpgradeRay.isPlaying)
                    {
                        m_UpgradeRay.Play();
                        m_RayImpact.Play();
                        m_RayImpact.GetComponent<AudioSource>().Play();
                    }
                }
                else
                {
                    StopUpgrading();
                }
            }
            else
            {
                StopUpgrading();
            }
        }

        if(Input.GetKey(KeyCode.F) )
        {
            EventManager.Instance.OnDeploymentStart.Invoke();
            Debug.Log("Deploying");
        }

        //Cancel placing / remove placed turret
        if (Input.GetKeyUp(KeyCode.Q))
        {
            GameObject currentObject = PlayerView.GameObject;
            if (currentObject != null)
            {
                BaseDeployable deployable = currentObject.transform.root.GetComponent<BaseDeployable>();

                if (deployable != null && deployable.IsDeployed() && deployable.curLevel == 0)
                {
                    Destroy(deployable.gameObject);
                }
                else
                {
                    EventManager.Instance.OnDeploymentCancel.Invoke();
                }
            }
        }

        //End Upgrading and placing
        if (Input.GetKeyUp(KeyCode.F))
        { 
            EventManager.Instance.OnDeploymentEnd.Invoke();
        }

        if (Input.GetKeyUp(KeyCode.E))
        {
            StopUpgrading();
        }

	}

    void StopUpgrading()
    {
        m_UpgradeRay.Clear();
        m_UpgradeRay.Stop();
        m_RayImpact.Stop();
        m_RayImpact.GetComponent<AudioSource>().Stop();

        EventManager.Instance.OnUpgradingStopped.Invoke();
    }

    public int GetCurrencyAmount()
    {
        return m_CurrencyAmount;
    }

    public void SetCurrencyAmount(int amount)
    {
        m_CurrencyAmount = amount;
    }

    public Collider GetCollider()
    {
        return m_Collider;
    }

    public void SetFirstPersonControllerActive(bool active)
    {
        m_FpsController.enabled = active;
    }

    public bool IsFirstPersonControllerActive()
    {
        return m_FpsController.enabled;
    }
}

using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System;

namespace RandomchaosLtd.Distingo
{
    [CustomEditor(typeof(DistingoTerrainScript))]
    public class DistingoTerrainShaderEditor : Editor
    {
        List<bool> ShowChannel = new List<bool>();
        
        DistingoTerrainScript thisDistingoTerrainScript;

        GUIStyle headingStyle;
        GUIStyle style;
        GUIStyle textArea;

        bool ShowGlobalSettings = false;

        void OnEnable()
        {
            
            //ShowChannel = new List<bool>();

            thisDistingoTerrainScript = (DistingoTerrainScript)target;
            
            SetShowChannels();

        }

        void SetShowChannels()
        {
            ShowChannel.Clear();
            if (!thisDistingoTerrainScript.IsMeshTerrain)
            {
                for (int t = 0; t < Mathf.Clamp(thisDistingoTerrainScript.GetComponent<Terrain>().terrainData.splatPrototypes.Length,0,4); t++)
                {
                    ShowChannel.Add(false);
                }
            }
            else
            {
                for (int t = 0; t < 4; t++)
                {
                    ShowChannel.Add(false);
                }
            }

            ShowGlobalSettings = false;
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            headingStyle = new GUIStyle();
            headingStyle.alignment = TextAnchor.MiddleLeft;
            headingStyle.normal.textColor = GUI.skin.label.normal.textColor;     

            
            style = new GUIStyle();
            style.alignment = TextAnchor.MiddleLeft;
            style.fontSize = 18;
            style.fontStyle = FontStyle.Italic;
            style.normal.textColor = GUI.skin.label.normal.textColor;

            textArea = GUI.skin.textArea;
            textArea.richText = true;

            string ChannelName = string.Empty;

            if (Application.HasProLicense())
                EditorGUILayout.LabelField(new GUIContent(Resources.Load<Texture2D>("distingo_dark")), headingStyle, GUILayout.Height(64));
            else
                EditorGUILayout.LabelField(new GUIContent(Resources.Load<Texture2D>("distingo_light")), GUILayout.Height(64));

            headingStyle.alignment = TextAnchor.MiddleCenter;


            EditorGUILayout.LabelField(string.Format("Version {0}", DistingoTerrainScript.Version), style);
            style.alignment = TextAnchor.MiddleCenter;


            thisDistingoTerrainScript.SelectedShader = thisDistingoTerrainScript.ShaderList[EditorGUILayout.Popup("Shader Type", thisDistingoTerrainScript.ShaderList.IndexOf(thisDistingoTerrainScript.SelectedShader), thisDistingoTerrainScript.ShaderList.ToArray())];

             if (!thisDistingoTerrainScript.IsMeshTerrain && ShowChannel.Count != Mathf.Clamp(thisDistingoTerrainScript.GetComponent<Terrain>().terrainData.splatPrototypes.Length,0,4))
                SetShowChannels();

            //if (thisDistingoTerrainScript.SelectedShader().Contains("Mobile"))
            //    EditorGUILayout.TextArea("<b>Mobile Version</b>\nThis version is trimmed down. It has a minimum requirement of SM3 and has calculations for Fog removed", textArea, GUILayout.MaxWidth(500));

            if (thisDistingoTerrainScript.SelectedShader.Contains("Occlusion"))
                EditorGUILayout.TextArea("<b>Occlusion Shaders</b>\nOcclusion shaders require that the Lightmap static be disabled as they will run out of texture registers, please see documentation for more details.", textArea, GUILayout.MaxWidth(500));

            if (GUILayout.Button(new GUIContent("Global Settings", "Edit parameters that affect all textures")))
                ShowGlobalSettings = !ShowGlobalSettings;

            
            if (ShowGlobalSettings)
            {
                serializedObject.FindProperty("UpdateShadersInEditor").boolValue = EditorGUILayout.Toggle(new GUIContent("Make shader updates in editor", "If true the shaders are updated every frame with the given settings."), serializedObject.FindProperty("UpdateShadersInEditor").boolValue);
                
                // Generic
                switch (thisDistingoTerrainScript.SelectedShader)
                {
                    case "Unity Terrain Occlusion Standard PBR":
                    case "Unity Terrain Occlusion Standard Specular PBR":
                    case "Unity Terrain Blend Standard PBR":
                    case "Unity Terrain Blend Standard Specular PBR":
                    //case "Mobile Unity Terrain Standard PBR":
                    //case "Mobile Unity Terrain Standard Specular PBR":
                    
                        EditorGUILayout.Slider(serializedObject.FindProperty("SplattingDistance"), 0, 10000, new GUIContent("Base Map Dist.", "This alters the terrains basemapDistance"));
                        serializedObject.FindProperty("UseNormalShader").boolValue = EditorGUILayout.Toggle(new GUIContent("Use Unity Shader", "Set this to true to use the built in Unity Shaders"), serializedObject.FindProperty("UseNormalShader").boolValue);

                        EditorGUILayout.Slider(serializedObject.FindProperty("MinMaxUVCutoff"), 0, 2, new GUIContent("Min Max UV CutOff", "Distance from camera where the switch between near and far UV multiplier occur"));
                        EditorGUILayout.Slider(serializedObject.FindProperty("DistanceOffSet"), 0, 1, new GUIContent("Distance Offset", "Distance offset"));
                        serializedObject.FindProperty("ShowFallOff").boolValue = EditorGUILayout.Toggle(new GUIContent("Render CutOff", "Show the cutoff in the render"), serializedObject.FindProperty("ShowFallOff").boolValue);
                        break;
                    case "Mesh Terrain Occlusion Standard PBR":
                    case "Mesh Terrain Occlusion Standard Specular PBR":
                    case "Mesh Terrain Blend Standard PBR":
                    case "Mesh Terrain Blend Standard Specular PBR":
                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField("Splat Map", headingStyle, GUILayout.Width(64));
                        if (thisDistingoTerrainScript.SelectedShader.Contains("Blend"))
                        {
                            EditorGUILayout.LabelField("Blend", headingStyle, GUILayout.Width(64));
                            EditorGUILayout.LabelField("Occlusion", headingStyle, GUILayout.Width(64));
                        }
                        EditorGUILayout.EndHorizontal();

                        EditorGUILayout.BeginHorizontal();
                        thisDistingoTerrainScript.tm_SplatMap = (Texture2D)EditorGUILayout.ObjectField(thisDistingoTerrainScript.tm_SplatMap, typeof(Texture2D), false, GUILayout.Height(64), GUILayout.Width(64));
                        if (thisDistingoTerrainScript.SelectedShader.Contains("Blend"))
                        {
                            thisDistingoTerrainScript.GlobalBlendTexture = (Texture2D)EditorGUILayout.ObjectField(thisDistingoTerrainScript.GlobalBlendTexture, typeof(Texture2D), false, GUILayout.Height(64), GUILayout.Width(64));
                            thisDistingoTerrainScript.GlobalOcclusionTexture = (Texture2D)EditorGUILayout.ObjectField(thisDistingoTerrainScript.GlobalOcclusionTexture, typeof(Texture2D), false, GUILayout.Height(64), GUILayout.Width(64));
                        }
                        EditorGUILayout.EndHorizontal();

                        if (thisDistingoTerrainScript.SelectedShader.Contains("Blend"))
                        {
                            EditorGUILayout.Slider(serializedObject.FindProperty("BlendPower"), 0, 2, new GUIContent("Blend Power", "The amount the texture is blended with the terrain."));
                            EditorGUILayout.Slider(serializedObject.FindProperty("OcclusionPower"), 0, 1, new GUIContent("Occlusion Power", "The amount of occlusion applied."));
                        }

                        EditorGUILayout.Slider(serializedObject.FindProperty("MinMaxUVCutoff"), 0, 2, new GUIContent("Min Max UV CutOff", "Distance from camera where the switch between near and far UV multiplier occur"));
                        EditorGUILayout.Slider(serializedObject.FindProperty("DistanceOffSet"), 0, 1, new GUIContent("Distance Offset", "Distance offset"));
                        serializedObject.FindProperty("ShowFallOff").boolValue = EditorGUILayout.Toggle(new GUIContent("Render CutOff", "Show the cutoff in the render"), serializedObject.FindProperty("ShowFallOff").boolValue);

                        break;

                }

                // Specific
                switch (thisDistingoTerrainScript.SelectedShader)
                {
                    case "Unity Terrain Occlusion Standard PBR":
                    case "Unity Terrain Occlusion Standard Specular PBR":
                        serializedObject.FindProperty("useTriPlanar").boolValue = EditorGUILayout.Toggle(new GUIContent("Use Triplanar Mapping", "Use UV Free mapping to avoid texture stretchin on steep terrain"), serializedObject.FindProperty("useTriPlanar").boolValue);
                        if(thisDistingoTerrainScript.useTriPlanar)
                            EditorGUILayout.Slider(serializedObject.FindProperty("TriPlanerUVMultiplier"), 0, 1, new GUIContent("Triplanar Multiplier", "Base multiplier for Triplaner textures"));
                        break;
                    case "Unity Terrain Blend Standard PBR":
                    case "Unity Terrain Blend Standard Specular PBR":
                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField("Blend", headingStyle, GUILayout.Width(64));
                        EditorGUILayout.LabelField("Occlusion", headingStyle, GUILayout.Width(64));
                        EditorGUILayout.EndHorizontal();

                        EditorGUILayout.BeginHorizontal();
                        thisDistingoTerrainScript.GlobalBlendTexture = (Texture2D)EditorGUILayout.ObjectField(thisDistingoTerrainScript.GlobalBlendTexture, typeof(Texture2D), false, GUILayout.Height(64), GUILayout.Width(64));
                        thisDistingoTerrainScript.GlobalOcclusionTexture = (Texture2D)EditorGUILayout.ObjectField(thisDistingoTerrainScript.GlobalOcclusionTexture, typeof(Texture2D), false, GUILayout.Height(64), GUILayout.Width(64));
                        EditorGUILayout.EndHorizontal();

                        EditorGUILayout.Slider(serializedObject.FindProperty("BlendPower"), 0, 2, new GUIContent("Blend Power", "The amount the texture is blended with the terrain."));
                        EditorGUILayout.Slider(serializedObject.FindProperty("OcclusionPower"), 0, 1, new GUIContent("Occlusion Power", "The amount of occlusion applied."));
                        serializedObject.FindProperty("useTriPlanar").boolValue = EditorGUILayout.Toggle(new GUIContent("Use Triplanar Mapping", "Use UV Free mapping to avoid texture stretchin on steep terrain"), serializedObject.FindProperty("useTriPlanar").boolValue);
                        if (thisDistingoTerrainScript.useTriPlanar)
                            EditorGUILayout.Slider(serializedObject.FindProperty("TriPlanerUVMultiplier"), 0, 1, new GUIContent("Triplanar Multiplier", "Base multiplier for Triplaner textures"));
                        break;
                    
                    case "Mesh Terrain Blend Standard PBR":
                    case "Mesh Terrain Blend Standard Specular PBR":
                    //case "Mobile Unity Terrain Standard PBR":
                    //case "Mobile Unity Terrain Standard Specular PBR":
                    case "Mesh Terrain Occlusion Standard PBR":
                    case "Mesh Terrain Occlusion Standard Specular PBR":
                        serializedObject.FindProperty("useTriPlanar").boolValue = EditorGUILayout.Toggle(new GUIContent("Use Triplanar Mapping", "Use UV Free mapping to avoid texture stretchin on steep terrain"), serializedObject.FindProperty("useTriPlanar").boolValue);
                        if (thisDistingoTerrainScript.useTriPlanar)
                            EditorGUILayout.Slider(serializedObject.FindProperty("TriPlanerUVMultiplier"), 0, 1, new GUIContent("Triplanar Multiplier", "Base multiplier for Triplaner textures"));
                        break;
                }
            }
            // Channels
            for (int c = 0; c < ShowChannel.Count; c++)
            {
                ChannelName = string.Format("Texture Channel {0}", c);
                if (GUILayout.Button(new GUIContent(ChannelName, string.Format("Terrain Texture Channel {0} parameters", c))))
                    ShowChannel[c] = !ShowChannel[c];

                if (ShowChannel[c])
                {
                    int tc = c;

                    switch (thisDistingoTerrainScript.SelectedShader)
                    {
                        case "Unity Terrain Occlusion Standard PBR":
                        case "Unity Terrain Occlusion Standard Specular PBR":
                        case "Mesh Terrain Occlusion Standard PBR":
                        case "Mesh Terrain Occlusion Standard Specular PBR":
                        case "Unity Terrain Blend Standard PBR":
                        case "Unity Terrain Blend Standard Specular PBR":
                        case "Mesh Terrain Blend Standard PBR":
                        case "Mesh Terrain Blend Standard Specular PBR":

                            if (thisDistingoTerrainScript.SelectedShader.Contains("Mesh"))
                            {
                                EditorGUILayout.BeginHorizontal();
                                EditorGUILayout.LabelField(new GUIContent("Material", "Apply a material to a texture channle, it must have a mainTexture, _BumpMap and _OcclusionMap. A Substance material can also be used."), headingStyle, GUILayout.Width(64));
                                thisDistingoTerrainScript.ChannelMaterials[c] = (Material)EditorGUILayout.ObjectField(thisDistingoTerrainScript.ChannelMaterials[c], typeof(Material), false);
                                EditorGUILayout.EndHorizontal();
                            }

                            if (thisDistingoTerrainScript.ChannelMaterials[c] == null) 
                            {
                                EditorGUILayout.BeginHorizontal();

                                EditorGUILayout.LabelField(new GUIContent("Splat", "This is the splat texture set up in your terrain, it can not be edited here."), headingStyle, GUILayout.Width(64));
                                EditorGUILayout.LabelField(new GUIContent("Normal", "This is the normal map set up in your terrain, it can not be edited here."), headingStyle, GUILayout.Width(64));
                                if (thisDistingoTerrainScript.SelectedShader.Contains("Occlusion"))
                                {
                                    EditorGUILayout.LabelField(new GUIContent("Occlusion", "This is the occlusion map to be applied to this texture channel, this can be edited here."), headingStyle, GUILayout.Width(64));
                                }
                                EditorGUILayout.EndHorizontal();



                                EditorGUILayout.BeginHorizontal();
                                if (thisDistingoTerrainScript.SelectedShader.Contains("Unity Terrain"))
                                {
                                    EditorGUILayout.ObjectField(thisDistingoTerrainScript.GetComponent<Terrain>().terrainData.splatPrototypes[c].texture, typeof(Texture2D), false, GUILayout.Height(64), GUILayout.Width(64));
                                    EditorGUILayout.ObjectField(thisDistingoTerrainScript.GetComponent<Terrain>().terrainData.splatPrototypes[c].normalMap, typeof(Texture2D), false, GUILayout.Height(64), GUILayout.Width(64));
                                }
                                else
                                {
                                    thisDistingoTerrainScript.tm_Splat[c] = (Texture2D)EditorGUILayout.ObjectField(thisDistingoTerrainScript.tm_Splat[c], typeof(Texture2D), false, GUILayout.Height(64), GUILayout.Width(64));
                                    thisDistingoTerrainScript.tm_Normal[c] = (Texture2D)EditorGUILayout.ObjectField(thisDistingoTerrainScript.tm_Normal[c], typeof(Texture2D), false, GUILayout.Height(64), GUILayout.Width(64));
                                }

                                if (thisDistingoTerrainScript.SelectedShader.Contains("Occlusion"))
                                    thisDistingoTerrainScript.OcclusionMaps[c] = (Texture2D)EditorGUILayout.ObjectField(thisDistingoTerrainScript.OcclusionMaps[c], typeof(Texture2D), false, GUILayout.Height(64), GUILayout.Width(64));

                                EditorGUILayout.EndHorizontal();
                            }

                            if (thisDistingoTerrainScript.SelectedShader.Contains("Mesh"))
                                thisDistingoTerrainScript.tmp_UVs[c] = EditorGUILayout.Vector2Field(new GUIContent("Base UV", ""), thisDistingoTerrainScript.tmp_UVs[c]);

                            
                            if (tc >= 4)
                                tc = (tc % 4);

                            EditorGUILayout.Slider(serializedObject.FindProperty(string.Format("BlendChannel{0}Max", tc)), DistingoTerrainScript.minUV, DistingoTerrainScript.maxUV, new GUIContent("Near UV Multiplier", "Number used to multiply the UV's when near to the camera"));
                            EditorGUILayout.Slider(serializedObject.FindProperty(string.Format("BlendChannel{0}Min", tc)), DistingoTerrainScript.minUV, DistingoTerrainScript.maxUV, new GUIContent("Far UV Multiplier", "Number used to multiply the UV's when far from the camera"));
                            EditorGUILayout.Slider(serializedObject.FindProperty(string.Format("BlendNChannel{0}", tc)), DistingoTerrainScript.minN, DistingoTerrainScript.maxN, new GUIContent("Normal Power", "The amound the normal map is increased by."));

                            if (thisDistingoTerrainScript.SelectedShader.Contains("Occlusion"))
                                EditorGUILayout.Slider(serializedObject.FindProperty(string.Format("OcclusionPower{0}", tc)), 0, 1, new GUIContent("Occlusion Power", "Level off occlusion applied"));

                            EditorGUILayout.Slider(serializedObject.FindProperty(string.Format("Channel{0}Smoothness", tc)), 0, 1, new GUIContent("Smoothness", "Alter the smoothness of the texture"));
                            if (!thisDistingoTerrainScript.SelectedShader.Contains("Specular"))
                                EditorGUILayout.Slider(serializedObject.FindProperty(string.Format("Channel{0}Metalic", tc)), 0, 1, new GUIContent("Metallic", "Alter the metalic nature of the texture"));
                            else
                                EditorGUILayout.Slider(serializedObject.FindProperty(string.Format("Channel{0}Metalic", tc)), 0, 2, new GUIContent("Specular", "Alter the specular nature of the texture"));
                            
                            EditorGUILayout.Slider(serializedObject.FindProperty(string.Format("Brightness{0}", tc)), 0, 8, new GUIContent("Brightness", "Raise or lower how bright this texture is rendered"));
                            break;
                            //case "Mobile Unity Terrain Standard PBR":
                            //case "Mobile Unity Terrain Standard Specular PBR":
                            //    EditorGUILayout.BeginHorizontal();
                            //    EditorGUILayout.LabelField(new GUIContent("Splat", "This is the splat texture set up in your terrain, it can not be edited here."), headingStyle, GUILayout.Width(64));
                            //    EditorGUILayout.LabelField(new GUIContent("Normal", "This is the normal map set up in your terrain, it can not be edited here."), headingStyle, GUILayout.Width(64));
                            //    EditorGUILayout.EndHorizontal();

                            //    EditorGUILayout.BeginHorizontal();
                            //    EditorGUILayout.ObjectField(thisDistingoTerrainScript.GetComponent<Terrain>().terrainData.splatPrototypes[c].texture, typeof(Texture2D), false, GUILayout.Height(64), GUILayout.Width(64));
                            //    EditorGUILayout.ObjectField(thisDistingoTerrainScript.GetComponent<Terrain>().terrainData.splatPrototypes[c].normalMap, typeof(Texture2D), false, GUILayout.Height(64), GUILayout.Width(64));

                            //    EditorGUILayout.EndHorizontal();

                            //    if (tc >= 4)
                            //        tc = (tc % 4);

                            //    EditorGUILayout.Slider(serializedObject.FindProperty(string.Format("BlendChannel{0}Max", tc)), DistingoTerrainScript.minUV, DistingoTerrainScript.maxUV, new GUIContent("Near UV Multiplier", "Number used to multiply the UV's when near to the camera"));
                            //    EditorGUILayout.Slider(serializedObject.FindProperty(string.Format("BlendChannel{0}Min", tc)), DistingoTerrainScript.minUV, DistingoTerrainScript.maxUV, new GUIContent("Far UV Multiplier", "Number used to multiply the UV's when far from the camera"));
                            //    EditorGUILayout.Slider(serializedObject.FindProperty(string.Format("BlendNChannel{0}", tc)), DistingoTerrainScript.minN, DistingoTerrainScript.maxN, new GUIContent("Normal Power", "The amound the normal map is increased by."));

                            //    EditorGUILayout.Slider(serializedObject.FindProperty(string.Format("Channel{0}Smoothness", tc)), 0, 1, new GUIContent("Smoothness", "Alter the smoothness of the texture"));
                            //    if (!thisDistingoTerrainScript.SelectedShader().Contains("Specular"))
                            //        EditorGUILayout.Slider(serializedObject.FindProperty(string.Format("Channel{0}Metalic", tc)), 0, 1, new GUIContent("Metallic", "Alter the metalic nature of the texture"));
                            //    else
                            //        EditorGUILayout.Slider(serializedObject.FindProperty(string.Format("Channel{0}Metalic", tc)), 0, 2, new GUIContent("Specular", "Alter the specular nature of the texture"));

                            //    EditorGUILayout.Slider(serializedObject.FindProperty(string.Format("Brightness{0}", tc)), 0, 8, new GUIContent("Brightness", "Raise or lower how bright this texture is rendered"));
                            //    break;
                    }
                }
            }

            if (GUILayout.Button(new GUIContent("Distingo Forum", "Go to the latest forum Disitngo Forum post.")))
                Application.OpenURL("http://forum.unity3d.com/threads/released-distingo-terrain-in-detail.383275/page-999");

//#if !GAIA_PRESENT
            string gaiaLogo = "gaia_logo_dark BG";
            if (!Application.HasProLicense())
                gaiaLogo = "gaia_logo_light BG";

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.Space();

            if (GUILayout.Button(new GUIContent(Resources.Load<Texture2D>(gaiaLogo), "Click to buy a copy of Gaia now!"), GUILayout.Height(64), GUILayout.MaxWidth(250)))
                Application.OpenURL("https://www.assetstore.unity3d.com/en/#!/content/42618");

            EditorGUILayout.Space();
            EditorGUILayout.EndHorizontal();
//#endif

//            if (!IsTypeAvailable("WorldCreatorReference"))
            {
                string WClogo = "WorldCreator";
                EditorGUILayout.Space();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.Space();

                if (GUILayout.Button(new GUIContent(Resources.Load<Texture2D>(WClogo), "Click to buy a copy of World Creator now!"), GUILayout.Height(64), GUILayout.MaxWidth(250)))
                    Application.OpenURL("https://www.assetstore.unity3d.com/en/#!/content/55073");

                EditorGUILayout.Space();
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.Space();
                EditorGUILayout.Space();
            }

            EditorUtility.SetDirty(target);
            serializedObject.ApplyModifiedProperties();
        }

        protected static bool IsTypeAvailable(string name)
        {
            Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
            foreach (Assembly assembly in assemblies)
            {
                Type[] types = assembly.GetTypes();

                foreach (Type T in types)
                {
                    if (T.Name == name)
                        return true;
                }
            }

            return false;
        }

    }
}


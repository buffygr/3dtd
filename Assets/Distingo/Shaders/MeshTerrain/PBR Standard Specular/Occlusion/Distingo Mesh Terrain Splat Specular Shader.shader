﻿Shader "Nature/Terrain/Distingo Mesh Terrain Splat Specular" 
{
	Properties
	{
		_MainTex("Diffuse (RGB)", 2D) = "red" {}
		_Splat3("Layer 3 (A)", 2D) = "black" {}
		_Splat2("Layer 2 (B)", 2D) = "black" {}
		_Splat1("Layer 1 (G)", 2D) = "black" {}
		_Splat0("Layer 0 (R)", 2D) = "black" {}
		_Normal3("Normal 3 (A)", 2D) = "bump" {}
		_Normal2("Normal 2 (B)", 2D) = "bump" {}
		_Normal1("Normal 1 (G)", 2D) = "bump" {}
		_Normal0("Normal 0 (R)", 2D) = "bump" {}
	}
	SubShader
	{
		Tags
		{
			"Queue" = "Geometry-100"
			"RenderType" = "Opaque"
		}
		LOD 200

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf StandardSpecular vertex:SplatmapVert finalcolor:myfinal fullforwardshadows
		#pragma multi_compile_fog  
		#pragma target 4.0
		#include "../../PBR Standard/Occlusion/DistingoMeshTerrainSplatmapCommon.cginc" 

		float4 smoothness;
	float4 metallic;

		float4 normalMod = float4(1, 1, 1, 1);
		
		int ShowFallOff = 1;

		float DistanceOffSet;

		void surf(Input IN, inout SurfaceOutputStandardSpecular o)
		{
			half4 splat_control;
			half weight;
			fixed4 mixedDiffuse;
			half4 defaultSmoothness = smoothness;

			float3 pos = (IN.pos.xyz / IN.pos.w);
			float d = pow(pos.z, 255) + DistanceOffSet;

#if (UNITY_REVERSED_Z)
			d = saturate((.4 - (pos.z * 28))) + DistanceOffSet;
#endif
			float4 occlusion = 1;
			
			
			SplatmapMix(IN, defaultSmoothness, splat_control, weight, mixedDiffuse, o.Normal, UVMin, offset, normalMod, d, occlusion);

			o.Albedo = mixedDiffuse.rgb + (float4(0, d, 0, 1) * UVCutoff * ShowFallOff);
			o.Alpha = weight;
			o.Occlusion = mul(splat_control, occlusion);
			o.Smoothness = mixedDiffuse.a;
			o.Specular = mixedDiffuse.a * dot(splat_control, metallic);
		}

		void myfinal(Input IN, SurfaceOutputStandardSpecular o, inout fixed4 color)
		{
			SplatmapApplyWeight(color, o.Alpha);
			SplatmapApplyFog(color, IN);
		}

		ENDCG
	}
	FallBack "Diffuse"
}

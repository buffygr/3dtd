Shader "Hidden/TerrainEngine/Splatmap/Distingo Standard-AddPass" 
{
	Properties 
	{
		// set by terrain engine
		[HideInInspector] _Control ("Control (RGBA)", 2D) = "red" {}
		[HideInInspector] _Splat3 ("Layer 3 (A)", 2D) = "white" {}
		[HideInInspector] _Splat2 ("Layer 2 (B)", 2D) = "white" {}
		[HideInInspector] _Splat1 ("Layer 1 (G)", 2D) = "white" {}
		[HideInInspector] _Splat0 ("Layer 0 (R)", 2D) = "white" {}
		[HideInInspector] _Normal3 ("Normal 3 (A)", 2D) = "bump" {}
		[HideInInspector] _Normal2 ("Normal 2 (B)", 2D) = "bump" {}
		[HideInInspector] _Normal1 ("Normal 1 (G)", 2D) = "bump" {}
		[HideInInspector] _Normal0 ("Normal 0 (R)", 2D) = "bump" {}
		/*[HideInInspector] [Gamma] _Metallic0 ("Metallic 0", Range(0.0, 1.0)) = 0.0	
		[HideInInspector] [Gamma] _Metallic1 ("Metallic 1", Range(0.0, 1.0)) = 0.0	
		[HideInInspector] [Gamma] _Metallic2 ("Metallic 2", Range(0.0, 1.0)) = 0.0	
		[HideInInspector] [Gamma] _Metallic3 ("Metallic 3", Range(0.0, 1.0)) = 0.0
		[HideInInspector] _Smoothness0 ("Smoothness 0", Range(0.0, 1.0)) = 1.0	
		[HideInInspector] _Smoothness1 ("Smoothness 1", Range(0.0, 1.0)) = 1.0	
		[HideInInspector] _Smoothness2 ("Smoothness 2", Range(0.0, 1.0)) = 1.0	
		[HideInInspector] _Smoothness3 ("Smoothness 3", Range(0.0, 1.0)) = 1.0*/
	}

	SubShader 
	{
		Tags 
		{
			"SplatCount" = "4"
			"Queue" = "Geometry-99"
			"IgnoreProjector"="True"
			"RenderType" = "Opaque"
		}

		CGPROGRAM
		// As we can't blend normals in g-buffer, this shader will not work in standard deferred path. 
		// So we use exclude_path:deferred to force it to only use the forward path.
		#pragma surface surf StandardSpecular decal:add vertex:SplatmapVert finalprepass:SplatmapFinalPrepass finalgbuffer:SplatmapFinalGBuffer finalcolor:myfinal fullforwardshadows
		#pragma multi_compile_fog
		#pragma target 4.0
		// needs more than 8 texcoords
		#pragma exclude_renderers gles
		#include "UnityPBSLighting.cginc"

		#pragma multi_compile __ _TERRAIN_NORMAL_MAP

		#define TERRAIN_SPLAT_ADDPASS
		#define TERRAIN_STANDARD_SHADER
		#include "../../PBR Standard/Occlusion/DistingoTerrainSplatmapCommon.cginc"

		float4 smoothness;
	float4 metallic;

		float4 normalMod = float4(1, 1, 1, 1);
		float bmod = 1.5;

		int ShowFallOff = 1;
		float DistanceOffSet;

		void SplatmapFinalPrepass(Input IN, SurfaceOutputStandardSpecular o, inout fixed4 normalSpec)
		{
			normalSpec *= o.Alpha;
		}

		void SplatmapFinalGBuffer(Input IN, SurfaceOutputStandardSpecular o, inout half4 diffuse, inout half4 specSmoothness, inout half4 normal, inout half4 emission)
		{
			diffuse.rgb *= o.Alpha;
			specSmoothness *= o.Alpha;
			normal.rgb *= o.Alpha;
			emission *= o.Alpha;
		}

		void surf(Input IN, inout SurfaceOutputStandardSpecular o)
		{
			half4 splat_control;
			half weight;
			fixed4 mixedDiffuse;
			half4 defaultSmoothness = smoothness;
			
			float3 pos = (IN.pos.xyz / IN.pos.w);
			float d = pow(pos.z, 256) + DistanceOffSet;
			float4 occlusion = 1;

			SplatmapMix(IN, defaultSmoothness, splat_control, weight, mixedDiffuse, o.Normal, UVMin, offset, normalMod, d, occlusion);

			o.Albedo = mixedDiffuse.rgb + (float4(0, d, 0, 1) * UVCutoff * ShowFallOff * (1 - UseNormal));
			o.Alpha = weight;
			o.Occlusion = mul(splat_control, occlusion);
			o.Smoothness = mixedDiffuse.a;
			o.Specular = mixedDiffuse.a * dot(splat_control, metallic);

		}

		void myfinal(Input IN, SurfaceOutputStandardSpecular o, inout fixed4 color)
		{
			SplatmapApplyWeight(color, o.Alpha);
			SplatmapApplyFog(color, IN);
		}

		ENDCG
	}

	Fallback "Hidden/TerrainEngine/Splatmap/Distingo Diffuse-AddPass"
}

﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace RandomchaosLtd.Distingo
{
    [ExecuteInEditMode]
    [AddComponentMenu("Scripts/Randomchaos Ltd/Distingo/DistingoTerrainScript")]
    public class DistingoTerrainScript : MonoBehaviour, ITerrainShader
    {
        public List<string> ShaderList = new List<string>();
        public string SelectedShader;
        public string lastSelectedShader;

        public static string Version = "1.4.4";

        public float TriPlanerUVMultiplier = .1f;
        public bool useTriPlanar = false;

        public bool IsMeshTerrain = true;

        #region Unity Terrain Prameters
        public float SplattingDistance = 6000;

        public const float maxUV = 10;
        public const float minUV = .1f;

        public const float maxN = 5;
        public const float minN = .1f;

        public float DistanceOffSet;

        [Range(minUV, maxUV)]
        public float BlendChannel0Max = 1f;
        [Range(minUV, maxUV)]
        public float BlendChannel1Max = 1f;
        [Range(minUV, maxUV)]
        public float BlendChannel2Max = 1f;
        [Range(minUV, maxUV)]
        public float BlendChannel3Max = 1f;

        [Range(minUV, maxUV)]
        public float BlendChannel0Min = 1f;
        [Range(minUV, maxUV)]
        public float BlendChannel1Min = 1f;
        [Range(minUV, maxUV)]
        public float BlendChannel2Min = 1f;
        [Range(minUV, maxUV)]
        public float BlendChannel3Min = 1f;

        [Range(minN, maxN)]
        public float BlendNChannel0 = 1f;
        [Range(minN, maxN)]
        public float BlendNChannel1 = 1f;
        [Range(minN, maxN)]
        public float BlendNChannel2 = 1f;
        [Range(minN, maxN)]
        public float BlendNChannel3 = 1f;

        [Range(0, 1)]
        public float Channel0Smoothness = 0;
        [Range(0, 1)]
        public float Channel1Smoothness = 0;
        [Range(0, 1)]
        public float Channel2Smoothness = 0;
        [Range(0, 1)]
        public float Channel3Smoothness = 0;

        [Range(0, 1)]
        public float Channel0Metalic = 0;
        [Range(0, 1)]
        public float Channel1Metalic = 0;
        [Range(0, 1)]
        public float Channel2Metalic = 0;
        [Range(0, 1)]
        public float Channel3Metalic = 0;

        [Range(1, 10)]
        public float BrightnessMod = 1f;

        public bool UseNormalShader = false;

        [Range(0, 1)]
        public float MinMaxUVCutoff = 1f;

        public bool ShowFallOff = false;

        public Texture2D[] OcclusionMaps = new Texture2D[12];

        public Material[] ChannelMaterials = new Material[4]; // Max of 4.

        public float OcclusionPower0 = 1;
        public float OcclusionPower1 = 1;
        public float OcclusionPower2 = 1;
        public float OcclusionPower3 = 1;

        public float Brightness0 = 1;
        public float Brightness1 = 1;
        public float Brightness2 = 1;
        public float Brightness3 = 1;

        public Texture2D GlobalBlendTexture;
        public Texture2D GlobalOcclusionTexture;

        public bool HasGlobalBlend;
        public float BlendPower = 1;
        public float OcclusionPower = 1;
        bool WasBlend;
        bool WasStandard;

        //Material OcclusionMat;
        //Material BlendMat;

        public bool UpdateShadersInEditor = true;

        Terrain thisTerrain;

        Material currentMaterial
        {
            get
            {
                if (string.IsNullOrEmpty(SelectedShader))
                    SelectedShader = ShaderList[0];

                return Materials[SelectedShader];
            }
        }

        #endregion

        #region Terrain Mesh Parameters

        public Texture2D tm_SplatMap;

        public Texture2D[] tm_Splat = new Texture2D[4];
        public Texture2D[] tm_Normal = new Texture2D[4];
        public Vector2[] tmp_UVs = new Vector2[4] { new Vector2(5, 5), new Vector2(5, 5), new Vector2(5, 5), new Vector2(5, 5) };

        public bool UseSpeculerPBR;

        Material MeshTerrainSplatMat;
        #endregion

     

        public bool Mobile = false;
        bool WasMobile;

        

        Dictionary<string, Material> Materials = new Dictionary<string, Material>();

        void Start()
        {
            thisTerrain = GetComponent<Terrain>();

            SetUpMaterials();

            ApplyShaderVariables();
        }

        // Method kindly donated by Adam from Gaia :)
        public List<Type> GetTypesInNamespace(string nameSpace)
        {
            List<Type> matchingTypes = new List<Type>();

            int assyIdx, typeIdx;
            System.Type[] types;
            System.Reflection.Assembly[] assemblies = System.AppDomain.CurrentDomain.GetAssemblies();
            for (assyIdx = 0; assyIdx < assemblies.Length; assyIdx++)
            {
                if (assemblies[assyIdx].FullName.StartsWith("Assembly"))
                {
                    types = assemblies[assyIdx].GetTypes();
                    for (typeIdx = 0; typeIdx < types.Length; typeIdx++)
                    {
                        if (!string.IsNullOrEmpty(types[typeIdx].Namespace))
                        {
                            if (types[typeIdx].Namespace.StartsWith(nameSpace))
                            {
                                matchingTypes.Add(types[typeIdx]);
                            }
                        }
                    }
                }
            }
            return matchingTypes;
        }

        #region Force Unity to build with these shaders [Thanks Lennart Johansen]
        public Shader[] IncludedShaders = new Shader[0];

        void OnEnable()
        {
            if (IncludedShaders.Length == 0)
            {
                IncludedShaders = new Shader[8];

                IncludedShaders[0] = Shader.Find("Nature/Terrain/Distingo Standard");
                IncludedShaders[1] = Shader.Find("Nature/Terrain/Distingo Standard Specular");
                IncludedShaders[2] = Shader.Find("Nature/Terrain/Distingo StandardWithBlend");
                IncludedShaders[3] = Shader.Find("Nature/Terrain/Distingo Standard SpecularWithBlend");
                IncludedShaders[4] = Shader.Find("Nature/Terrain/Distingo Mesh Terrain Splat");
                IncludedShaders[5] = Shader.Find("Nature/Terrain/Distingo Mesh Terrain Splat Specular");
                IncludedShaders[6] = Shader.Find("Nature/Terrain/Distingo Mesh Terrain Splat Blend");
                IncludedShaders[7] = Shader.Find("Nature/Terrain/Distingo Mesh Terrain Splat Specular Blend");
            }
        }
        #endregion

        void SetUpMaterials()
        {
            Materials.Clear();

            // Set up stock list
            ShaderList = new List<string>();

            ShaderList.Add("Unity Terrain Occlusion Standard PBR");
            ShaderList.Add("Unity Terrain Occlusion Standard Specular PBR");
            ShaderList.Add("Unity Terrain Blend Standard PBR");
            ShaderList.Add("Unity Terrain Blend Standard Specular PBR");
            ShaderList.Add("Mesh Terrain Occlusion Standard PBR");
            ShaderList.Add("Mesh Terrain Occlusion Standard Specular PBR");
            ShaderList.Add("Mesh Terrain Blend Standard PBR");
            ShaderList.Add("Mesh Terrain Blend Standard Specular PBR");
            //ShaderList.Add("Mobile Unity Terrain Standard PBR");
            //ShaderList.Add("Mobile Unity Terrain Standard Specular PBR");

            if (string.IsNullOrEmpty(SelectedShader))
                SelectedShader = ShaderList[0];

            Materials.Add(ShaderList[0], new Material(Shader.Find("Nature/Terrain/Distingo Standard")));
            Materials.Add(ShaderList[1], new Material(Shader.Find("Nature/Terrain/Distingo Standard Specular")));

            Materials.Add(ShaderList[2], new Material(Shader.Find("Nature/Terrain/Distingo StandardWithBlend")));
            Materials.Add(ShaderList[3], new Material(Shader.Find("Nature/Terrain/Distingo Standard SpecularWithBlend")));

            Materials.Add(ShaderList[4], new Material(Shader.Find("Nature/Terrain/Distingo Mesh Terrain Splat")));
            Materials.Add(ShaderList[5], new Material(Shader.Find("Nature/Terrain/Distingo Mesh Terrain Splat Specular")));

            Materials.Add(ShaderList[6], new Material(Shader.Find("Nature/Terrain/Distingo Mesh Terrain Splat Blend")));
            Materials.Add(ShaderList[7], new Material(Shader.Find("Nature/Terrain/Distingo Mesh Terrain Splat Specular Blend")));

            //Materials.Add("ShaderList[8]", new Material(Shader.Find("Nature/Terrain/Distingo Mobile")));
            //Materials.Add("ShaderList[9]", new Material(Shader.Find("Nature/Terrain/Distingo Specular Mobile")));
            

        }

        void Update()
        {
            if (Application.isEditor && UpdateShadersInEditor)
                ApplyShaderVariables();
        }

        void SetUpUnityTerrain(Material material)
        {
            if (thisTerrain.materialType != Terrain.MaterialType.Custom)
                thisTerrain.materialType = Terrain.MaterialType.Custom;

            thisTerrain.materialTemplate = material;


        }


        void InitializeDistingo()
        {
            // Set selected 
            switch (SelectedShader)
            {
                case "Unity Terrain Occlusion Standard PBR":
                case "Unity Terrain Occlusion Standard Specular PBR":
                case "Unity Terrain Blend Standard PBR":
                case "Unity Terrain Blend Standard Specular PBR":
                case "Mobile Unity Terrain Standard PBR":
                case "Mobile Unity Terrain Standard Specular PBR":
               
                    if (Materials.ContainsKey(ShaderList[ShaderList.IndexOf(SelectedShader)]))
                        SetUpUnityTerrain(Materials[ShaderList[ShaderList.IndexOf(SelectedShader)]]);

                    if (thisTerrain == null)
                        thisTerrain = GetComponent<Terrain>();
                    break;
                case "Mesh Terrain Occlusion Standard PBR":
                case "Mesh Terrain Occlusion Standard Specular PBR":
                case "Mesh Terrain Blend Standard PBR":
                case "Mesh Terrain Blend Standard Specular PBR":
                    GetComponent<MeshRenderer>().sharedMaterial = Materials[ShaderList[ShaderList.IndexOf(SelectedShader)]];
                    break;
            }
        }

        void ApplyShaderVariables()
        {
            if (Materials.Count == 0)
                SetUpMaterials();

            if (gameObject.GetComponent<Terrain>() != null)
                IsMeshTerrain = false;

            if (SelectedShader.ToString().Contains("Mesh") && !IsMeshTerrain)
                SelectedShader = ShaderList[0];

            if (!SelectedShader.ToString().Contains("Mesh") && IsMeshTerrain)
                SelectedShader = ShaderList[4];

            InitializeDistingo();

            // Update Params Same for all types
            switch (SelectedShader)
            {
                case "Unity Terrain Occlusion Standard PBR":
                case "Unity Terrain Occlusion Standard Specular PBR":
                case "Unity Terrain Blend Standard PBR":
                case "Unity Terrain Blend Standard Specular PBR":
                case "Mobile Unity Terrain Standard PBR":
                case "Mobile Unity Terrain Standard Specular PBR":
                    thisTerrain.basemapDistance = SplattingDistance;

                    currentMaterial.SetFloat("DistanceOffSet", DistanceOffSet);

                    //Shader.SetGlobalVector("offset", new Vector4(BlendChannel0Max, BlendChannel1Max, BlendChannel2Max, BlendChannel3Max));
                    //thisTerrain.materialTemplate.SetVector("offset", new Vector4(BlendChannel0Max, BlendChannel1Max, BlendChannel2Max, BlendChannel3Max));
                    currentMaterial.SetVector("offset", new Vector4(BlendChannel0Max, BlendChannel1Max, BlendChannel2Max, BlendChannel3Max));

                    currentMaterial.SetVector("UVMin", new Vector4(BlendChannel0Min, BlendChannel1Min, BlendChannel2Min, BlendChannel3Min));
                    currentMaterial.SetVector("normalMod", new Vector4(BlendNChannel0, BlendNChannel1, BlendNChannel2, BlendNChannel3));

                    currentMaterial.SetVector("_Brightness", new Vector4(Brightness0, Brightness1, Brightness2, Brightness3));

                    currentMaterial.SetInt("ShowFallOff", ShowFallOff ? 1 : 0);

                    
                    currentMaterial.SetVector("smoothness", new Vector4(Channel0Smoothness, Channel1Smoothness, Channel2Smoothness, Channel3Smoothness));
                    currentMaterial.SetVector("metallic", new Vector4(Channel0Metalic, Channel1Metalic, Channel2Metalic, Channel3Metalic));

                    currentMaterial.SetFloat("UVCutoff", MinMaxUVCutoff);

                    currentMaterial.SetFloat("bmod", BrightnessMod);
                    currentMaterial.SetInt("UseNormal", UseNormalShader ? 1 : 0);
                    currentMaterial.SetInt("UseTriPlanar", useTriPlanar ? 1 : 0);
                    currentMaterial.SetFloat("triPlanerMultiplier", TriPlanerUVMultiplier);
                    break;
                case "Mesh Terrain Occlusion Standard PBR":
                case "Mesh Terrain Occlusion Standard Specular PBR":
                case "Mesh Terrain Blend Standard PBR":
                case "Mesh Terrain Blend Standard Specular PBR":

                    currentMaterial.SetFloat("DistanceOffSet", DistanceOffSet);
                    currentMaterial.SetTexture("_MainTex", tm_SplatMap);

                    for (int t = 0; t < tm_Splat.Length; t++)
                    {
                        currentMaterial.SetVector(string.Format("_Splat{0}UV", t), tmp_UVs[t]);

                        if (ChannelMaterials[t] != null)
                        {
                            tm_Normal[t] = null;
                            tm_Splat[t] = null;
                            currentMaterial.SetTexture(string.Format("_Normal{0}", t), ChannelMaterials[t].GetTexture("_BumpMap"));
                            currentMaterial.SetTexture(string.Format("_Splat{0}", t), ChannelMaterials[t].mainTexture);
                        }
                        else
                        {
                            currentMaterial.SetTexture(string.Format("_Normal{0}", t), tm_Normal[t]);
                            currentMaterial.SetTexture(string.Format("_Splat{0}", t), tm_Splat[t]);
                        }
                    }

                    currentMaterial.SetVector("offset", new Vector4(BlendChannel0Max, BlendChannel1Max, BlendChannel2Max, BlendChannel3Max));
                    currentMaterial.SetVector("UVMin", new Vector4(BlendChannel0Min, BlendChannel1Min, BlendChannel2Min, BlendChannel3Min));
                    currentMaterial.SetVector("normalMod", new Vector4(BlendNChannel0, BlendNChannel1, BlendNChannel2, BlendNChannel3));

                    currentMaterial.SetVector("_Brightness", new Vector4(Brightness0, Brightness1, Brightness2, Brightness3));

                    currentMaterial.SetInt("ShowFallOff", ShowFallOff ? 1 : 0);

                    currentMaterial.SetVector("smoothness", new Vector4(Channel0Smoothness, Channel1Smoothness, Channel2Smoothness, Channel3Smoothness));
                    currentMaterial.SetVector("metallic", new Vector4(Channel0Metalic, Channel1Metalic, Channel2Metalic, Channel3Metalic));

                    currentMaterial.SetFloat("UVCutoff", MinMaxUVCutoff);

                    currentMaterial.SetFloat("bmod", BrightnessMod);

                    currentMaterial.SetInt("UseTriPlanar", useTriPlanar ? 1 : 0);
                    currentMaterial.SetFloat("triPlanerMultiplier", TriPlanerUVMultiplier);

                    break;
            }

            // Specifics
            switch (SelectedShader)
            {
                case "Unity Terrain Occlusion Standard PBR":
                case "Unity Terrain Occlusion Standard Specular PBR":
                    if (OcclusionMaps != null && OcclusionMaps.Length > 0)
                    {
                        for (int h = 0; h < OcclusionMaps.Length; h++)
                        {
                            currentMaterial.SetTexture(string.Format("_OcculisonTex{0}", h), OcclusionMaps[h]);
                            currentMaterial.SetInt(string.Format("occ{0}", h), OcclusionMaps[h] != null ? 1 : 0);
                        }
                    }
                    else
                    {
                        currentMaterial.SetInt("occ0", 0);
                        currentMaterial.SetInt("occ1", 0);
                        currentMaterial.SetInt("occ2", 0);
                        currentMaterial.SetInt("occ3", 0);
                    }

                    currentMaterial.SetVector("_OcclusionPower", new Vector4(OcclusionPower0, OcclusionPower1, OcclusionPower2, OcclusionPower3));

                    break;
                case "Unity Terrain Blend Standard PBR":
                case "Unity Terrain Blend Standard Specular PBR":
                    currentMaterial.SetInt("doGlobalBelnd", GlobalBlendTexture != null ? 1 : 0);
                    currentMaterial.SetTexture("GlobalBlend", GlobalBlendTexture);
                    currentMaterial.SetFloat("BlendPower", BlendPower);

                    currentMaterial.SetInt("doGlobalOcclude", GlobalOcclusionTexture != null ? 1 : 0);
                    currentMaterial.SetTexture("OcclusionBlend", GlobalOcclusionTexture);
                    currentMaterial.SetFloat("OccludePower", OcclusionPower);
                    break;
                case "Mobile Unity Terrain Standard PBR":
                case "Mobile Unity Terrain Standard Specular PBR":
                    // None
                    break;
                case "Mesh Terrain Occlusion Standard PBR":
                case "Mesh Terrain Occlusion Standard Specular PBR":
                    if (OcclusionMaps != null && OcclusionMaps.Length > 0)
                    {
                        for (int h = 0; h < OcclusionMaps.Length; h++)
                        {
                            if (h < 4 && ChannelMaterials[h] != null)
                            {
                                currentMaterial.SetTexture(string.Format("_OcculisonTex{0}", h), ChannelMaterials[h].GetTexture("_OcclusionMap"));
                                currentMaterial.SetInt(string.Format("occ{0}", h), OcclusionMaps[h] != null ? 1 : 0);
                            }
                            else
                            {
                                currentMaterial.SetTexture(string.Format("_OcculisonTex{0}", h), OcclusionMaps[h]);
                                currentMaterial.SetInt(string.Format("occ{0}", h), OcclusionMaps[h] != null ? 1 : 0);
                            }
                        }
                    }
                    else
                    {
                        currentMaterial.SetInt("occ0", 0);
                        currentMaterial.SetInt("occ1", 0);
                        currentMaterial.SetInt("occ2", 0);
                        currentMaterial.SetInt("occ3", 0);
                    }

                    currentMaterial.SetVector("_OcclusionPower", new Vector4(OcclusionPower0, OcclusionPower1, OcclusionPower2, OcclusionPower3));
                    break;
                case "Mesh Terrain Blend Standard PBR":
                case "Mesh Terrain Blend Standard Specular PBR":
                    currentMaterial.SetInt("doGlobalBelnd", GlobalBlendTexture != null ? 1 : 0);
                    currentMaterial.SetTexture("GlobalBlend", GlobalBlendTexture);
                    currentMaterial.SetFloat("BlendPower", BlendPower);

                    currentMaterial.SetInt("doGlobalOcclude", GlobalOcclusionTexture != null ? 1 : 0);
                    currentMaterial.SetTexture("OcclusionBlend", GlobalOcclusionTexture);
                    currentMaterial.SetFloat("OccludePower", OcclusionPower);
                    break;
            }

            lastSelectedShader = SelectedShader;

            if (thisTerrain != null)
            {
                thisTerrain.materialTemplate = null;

                SetUpUnityTerrain(currentMaterial);
            }
        }



        /// <summary>
        /// Distingo will not alter Terrain Engine Textures.
        /// Only occlusion textures are set with this method per channel.
        /// </summary>
        /// <param name="terrainTextureInfo">Array of texture data to set. Each array element is a texture channel, global textures need to be set with UpdateGlobalTextures
        /// near and far UV can be set with TerrainTextureInfo.uv, normal and occlusion power with TerrainTextureInfo.offset.
        /// </param>
        public void onTerrainTextureChange(TerrainTextureInfo[] terrainTextureInfo)
        {
            int maxChannel = terrainTextureInfo.Length;

            for (int c = 0; c < maxChannel; c++)
            {
                if(c < OcclusionMaps.Length-1)
                    OcclusionMaps[c] = terrainTextureInfo[c].Texture;
            }

            if (maxChannel > 0)
            {
                BlendChannel0Min = terrainTextureInfo[0].uv.x;
                BlendChannel0Max = terrainTextureInfo[0].uv.y;
                BlendNChannel0 = terrainTextureInfo[0].offset.x;
                OcclusionPower0 = terrainTextureInfo[0].offset.y;
                
            }

            if (maxChannel > 1)
            {
                BlendChannel1Min = terrainTextureInfo[1].uv.x;
                BlendChannel1Max = terrainTextureInfo[1].uv.y;
                BlendNChannel1 = terrainTextureInfo[1].offset.x;
                OcclusionPower1 = terrainTextureInfo[1].offset.y;

            }

            if (maxChannel > 2)
            {
                BlendChannel2Min = terrainTextureInfo[2].uv.x;
                BlendChannel2Max = terrainTextureInfo[2].uv.y;
                BlendNChannel2 = terrainTextureInfo[2].offset.x;
                OcclusionPower2 = terrainTextureInfo[2].offset.y;

            }

            if (maxChannel > 3)
            {
                BlendChannel3Min = terrainTextureInfo[3].uv.x;
                BlendChannel3Max = terrainTextureInfo[3].uv.y;
                BlendNChannel3 = terrainTextureInfo[3].offset.x;
                OcclusionPower3 = terrainTextureInfo[3].offset.y;

            }

            ApplyShaderVariables();
        }

        /// <summary>
        /// This method will update Distingo Global textures and their power.
        /// </summary>
        /// <param name="blendTexture"></param>
        /// <param name="occlusionTexture"></param>
        /// <param name="blendPower"></param>
        /// <param name="occlusionPower"></param>
        public void UpdateGlobalTextures(Texture2D blendTexture = null, Texture2D occlusionTexture = null, float blendPower = 0, float occlusionPower = 0)
        {
            GlobalBlendTexture = blendTexture;
            GlobalOcclusionTexture = occlusionTexture;
            BlendPower = blendPower;
            OcclusionPower = occlusionPower;

            ApplyShaderVariables();
        }
    }
}
    
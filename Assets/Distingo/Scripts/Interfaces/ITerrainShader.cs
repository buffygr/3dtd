﻿using UnityEngine;

[System.Serializable]
public class TerrainTextureInfo
{
    public Texture2D Texture;
    public Texture2D TextureNormals;
    public Texture2D TextureOcclusion;
    public Texture2D TextureHeightMap;
    public Vector2 uv;
    public Vector2 offset;
}

public interface ITerrainShader
{
    void onTerrainTextureChange(TerrainTextureInfo[] terrainTextureInfo);
}
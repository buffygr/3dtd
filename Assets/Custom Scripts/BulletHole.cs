﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class BulletHole : MonoBehaviour {

    public float maxAlive = 5f;
    private float m_curAlive;
    private Vector3 m_initialScale;
    private bool m_Dying;

	// Use this for initialization
	void Start ()
    {
        m_curAlive = 0;
        m_Dying = false;
	}
	
	// Update is called once per frame
	void Update ()
    {        
        m_curAlive += Time.deltaTime;
        if( m_curAlive >= maxAlive && !m_Dying )
        {
            m_Dying = true;
            transform.DOScale(Vector3.zero, 1f).SetEase(Ease.Linear).OnComplete(() => Destroy(gameObject));
        }
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseProjectile : MonoBehaviour
{

    // Use this for initialization
    public float lifeTime = 3f;
    public float speed = 10f;
    public float damage = 10f;
	public GameObject bloodSplatPrefab;
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        lifeTime -= Time.deltaTime;
        if (lifeTime <= 0)
            Destroy(gameObject);
    }

    void OnCollisionEnter(Collision collision)
    {
		ContactPoint hit = collision.contacts[0];
        Quaternion rotation = Quaternion.FromToRotation(Vector3.up, hit.normal);

        //check if is enemy
        BaseHostile hostile = collision.transform.root.GetComponent<BaseHostile>();
        if (hostile == null)
        {
            Destroy(gameObject);
            return;
        }

        hostile.HandleDamage(damage);
		Instantiate(bloodSplatPrefab, hit.point, rotation);

        Destroy(gameObject);
    }
}

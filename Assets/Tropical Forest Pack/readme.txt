Thank you for purchasing my Tropical Forest Pack. I hope it can help you with your project.

The folder system is setup 

Demo_Scene_Files is filled with different things that are needed for the demo scenes. Includes Audio files, scripts, and shaders.
	
	The three audio files are from Freesound.org. Edited by me in Audacity.
		
	Footstep_Sand: sand_step by pgi link- http://www.freesound.org/people/pgi/sounds/211457/
	jungle_ambience01: Tropical Birda by dubminister link- http://www.freesound.org/people/dubminister/sounds/214676/
	wind_in_trees: wind in trees from cover by strangy link- http://www.freesound.org/people/strangy/sounds/176253/

	Many thanks to them for their amazing work.
	
	CustomTreeImporterFiles included with permission from Lars. This is to keep the trees working.

The Import folder contains the different meshes. The colliders, particle, and tree imports.

Materials folder contains the materials for the main assets. Particle, Terrain, and Trees.

Prefabs folder. This is setup so you have your already setup prefabs for quick use. You have the Particles and Trees. 

	In the Trees folder you have all the trees you can input right in the terrain system and an LODs folder. Each tree has an enabled capsule collider for collision 
	on the terrain. Most of the trees also have a disabled Mesh Collider that is setup with the imported collider mesh. So if you are placing them 
	manually and not on the terrain you can disable the capsule collider and enable the mesh collider.

	In the LODs folder there is just one setup tree now. The Kapok_Tree01_LOD. It is setup with 6 LODs. From the main mesh to the billboard. This is good for manual 
	placement.

Textures folder. Contains all the textures for the pack. There are even terrain diffuse, normal map for the demo terrain. 

	There is an AFS_Textures folder which is not setup yet. Will be worked on for a future update.
	
	Most textures in this pack are from textures.com(formerly CGTextures)link- http://www.textures.com/
	The rest were made by me by either photography or modeled and rendered.

Then the two demo scenes.

Tropical Forest Pack is just for a quick walkthrough of all the assets included.

Tropical Forest Pack demo is a scene with a setup terrain for you to walk around to experience the pack in full or if you wish use for your project.

Known Issues:

	There is a problem with the Kapok Tree and the SSAO effect. I have no idea what is causing it but it just will not work with SSAO. When I do find the cause I will fix and update.
	
	If you are having conflicts with CTI-Camera-DepthNormalTexture and another asset contact me.

For any issues you can contact me on the thread that is linked in the Asset Store page or email me at baldinoboy@gmail.com

Thank you again for purchasing the Tropical Forest Pack.